﻿using System;

namespace SwapVariableValues
{
    public class Program
    {


        public (double, double) swap(double x, double y)
        {
            //Ergänzen sie ihren Code hier
           


            return (x, y);
        }



        static void Main(string[] args)
        {
            Program prg = new Program();

            // Die werte können je nach belieben gerne geändert werden
            double i = 10, j = 2;

            Console.WriteLine("Before Swap: " + (i, j));

            Console.WriteLine("After Swap:" + prg.swap(i, j));
        }
    } 
}
