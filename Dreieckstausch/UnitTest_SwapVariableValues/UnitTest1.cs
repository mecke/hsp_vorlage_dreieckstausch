using Microsoft.VisualStudio.TestTools.UnitTesting;
using SwapVariableValues;

namespace UnitTest_SwapVariableValues
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            //Arrange
            Program prg = new Program();

            //Act
            var result = prg.swap(12, 48);

            //Assert
            Assert.AreEqual(result, (48, 12));
        }
    }
}
